﻿using s1061666.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1061666.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index() {
            return View(new BMIData());
        }
        
        // GET: BMI
        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (data.Height != null && data.Weight != null) {
                var m_height = data.Height / 100;
                var result = data.Weight / (m_height * m_height);

                data.Result = result;
                data.Level = BMI_test(result);
            }
            return View(data);
        }
        private String BMI_test(float? val)
        {
            if (val < 18.5)
            {
                return "體重過輕";
            }
            else if (val >= 18.5 && val < 24)
            {
                return "正常範圍";
            }
            else if (val >= 24 && val < 27)
            {
                return "過重";
            }
            else if (val >= 27 && val < 30)
            {
                return "輕度肥胖";
            }
            else if (val >= 30 && val < 35)
            {
                return "中度肥胖";
            }
            else {
                return "重度肥胖";
            }
        }
    }
}