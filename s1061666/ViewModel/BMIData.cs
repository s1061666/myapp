﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace s1061666.ViewModel
{
    public class BMIData
    {
        public float? Weight { get; set;  }
        public float? Height { get; set; }
        public float? Result { get; set; }
        public string Level { get; set; }
    }
}